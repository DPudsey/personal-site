var animation = bodymovin.loadAnimation({
    container: document.getElementById('lottie-logo'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: 'assets/js/json/logo.json'
})