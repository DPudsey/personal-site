(function($, window, document, undefined) {
  "use strict";

  $(function() {
    // Add the Speech mark arrow
    $(".hero").addClass("hero--homepage-animation");
    $(".btn").prepend(
      '<div class="hover"><span></span><span></span><span></span><span></span><span></span></div>'
    );

    // Add class to ensure animation loads after page loads
    $(".hero-code__screen").addClass("hero-code__screen--animation");
    $(".hero--homepage h1").addClass("neon-highlight");

    // Add the confetti
    var animateButton = function(e) {
      e.preventDefault;
      //reset animation
      e.target.classList.remove("animate");

      e.target.classList.add("animate");
      setTimeout(function() {
        e.target.classList.remove("animate");
      }, 1700);
    };

    // My age
    function getAge(dateString) {
      var today = new Date();
      var birthDate = new Date(dateString);
      var age = today.getFullYear() - birthDate.getFullYear();
      var m = today.getMonth() - birthDate.getMonth();
      if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
          age--;
      }
      return age;
    }
    const myAge = document.getElementById('js-my-age');
    if(myAge) {
      myAge.innerHTML = getAge("1987/04/06");
    }

    // Easter egg
    var egg = new Egg();
    const html = document.querySelector("html");

    egg.addCode("c,r,a,b,s,o,n", function() {
      html.classList.add("senor-crabson");
    }).listen();

    egg.addCode("b,l,e,a,k", function() {
      html.classList.add("bleak");
    }).listen();

    // This allows the confetti button to be re-animated
    var classname = document.getElementsByClassName("confetti-button");
    var animateButton = function(e) {
      e.preventDefault;
      this.classList.remove("confetti-button");
      void this.offsetWidth;
      this.classList.add("confetti-button");
      this.classList.add("confetti-button--reanimate");
    }
    for (var i = 0; i < classname.length; i++) {
      classname[i].addEventListener("click", animateButton, false);
    }

    // Smooth scroll
    // Select all links with hashes
    $('a[href*="#"]')
      // Remove links that don't actually link to anything
      .not('[href="#"]')
      .not('[href="#0"]')
      .click(function(event) {
        // On-page links
        if (
          location.pathname.replace(/^\//, "") ==
            this.pathname.replace(/^\//, "") &&
          location.hostname == this.hostname
        ) {
          // Figure out element to scroll to
          var target = $(this.hash);
          target = target.length
            ? target
            : $("[name=" + this.hash.slice(1) + "]");
          // Does a scroll target exist?
          if (target.length) {
            // Only prevent default if animation is actually gonna happen
            event.preventDefault();
            $("html, body").animate(
              {
                scrollTop: target.offset().top
              },
              1000,
              function() {
                // Callback after animation
                // Must change focus!
                var $target = $(target);
                $target.focus();
                if ($target.is(":focus")) {
                  // Checking if the target was focused
                  return false;
                } else {
                  $target.attr("tabindex", "-1"); // Adding tabindex for elements not focusable
                  $target.focus(); // Set focus again
                }
              }
            );
          }
        }
      });

    // Add the particles
    // Set variables
    var canvasParticles = document.getElementById("particles");
    var ctxParticles = canvasParticles.getContext("2d");
    canvasParticles.width = window.innerWidth;
    canvasParticles.height = window.innerHeight;
    var particles = [];
    var num_particles = 100;

    // Random colour generator
    function GetRandomColor() {
      var r = 0,
        g = 0,
        b = 0;
      while (r < 100 && g < 100 && b < 100) {
        r = Math.floor(Math.random() * 256);
        g = Math.floor(Math.random() * 256);
        b = Math.floor(Math.random() * 256);
      }

      return "rgb(" + r + "," + g + "," + b + ")";
    }
    // Particle starting position and colour assignment
    var Particle = function() {
      this.x = canvasParticles.width * Math.random();
      this.y = canvasParticles.height * Math.random();
      this.vx = 4 * Math.random() - 2;
      this.vy = 4 * Math.random() - 2;
      this.Color = GetRandomColor();
    };

    Particle.prototype.Draw = function(ctxParticles) {
      ctxParticles.fillStyle = this.Color;
      ctxParticles.fillRect(this.x, this.y, 2, 2);
    };

    Particle.prototype.Update = function() {
      this.x += this.vx;
      this.y += this.vy;

      if (this.x < 0 || this.x > canvasParticles.width) this.vx = -this.vx;

      if (this.y < 0 || this.y > canvasParticles.height) this.vy = -this.vy;
    };

    function loop() {
      ctxParticles.clearRect(0, 0, canvasParticles.width, canvasParticles.height);

      for (var i = 0; i < num_particles; i++) {
        particles[i].Update();
        particles[i].Draw(ctxParticles);
      }
      requestAnimationFrame(loop);
    }

    // Create particles
    for (var i = 0; i < num_particles; i++) particles.push(new Particle());
    loop();

    // Canvas spinner
    var canvasLoader = document.getElementById('canvas--loader');
    if (canvasLoader) {
      var d = canvasLoader.width = canvasLoader.height = 120,
      ctxLoader = canvasLoader.getContext('2d'),
      opacity;
  
      ctxLoader.translate(d / 2, d / 2);
      ctxLoader.rotate(Math.PI * 360/360);
      ctxLoader.lineWidth = 4;
      ctxLoader.lineCap = 'butt';
  
      for(var i = 0; i <= 360; i++) {
          ctxLoader.save();
          ctxLoader.rotate((Math.PI * i/180));
          ctxLoader.beginPath();
          ctxLoader.moveTo(0, 0);
          opacity = (360 - (i * 1)) / 360;
          ctxLoader.strokeStyle = 'rgba(255,255,255,' + opacity.toFixed(2) +')'; 
          ctxLoader.lineTo(0, d + 30);
          ctxLoader.stroke();
          ctxLoader.closePath();
          ctxLoader.restore();
      }
  
      ctxLoader.globalCompositeOperation ='source-out';
      ctxLoader.beginPath();
      ctxLoader.arc(0, 0, d/2, 2 * Math.PI, false);
      ctxLoader.fillStyle = '#000';
      ctxLoader.fill();
  
      ctxLoader.globalCompositeOperation ='destination-out';
      ctxLoader.beginPath();
      ctxLoader.arc(0, 0, (d/2) * .9, 2 * Math.PI, false); 
      ctxLoader.fill();
    }
  });
})(jQuery, window, document);
